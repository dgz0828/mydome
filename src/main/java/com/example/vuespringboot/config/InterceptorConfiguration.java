package com.example.vuespringboot.config;

import com.example.vuespringboot.Interceptor.MyInterceptor;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfiguration implements WebMvcConfigurer {


/*  @Bean
  public RedissonClient getRedissonClient(){


    return Redisson.create();


  }*/


  @Bean
  public MyInterceptor getMyInterceptor() {
    return new MyInterceptor();
  }


  @Override
  public void addInterceptors(InterceptorRegistry registry) {

    InterceptorRegistration interceptor = registry.addInterceptor(getMyInterceptor());

    //需要拦截的路径
    interceptor.addPathPatterns("/list");
    //不需要拦截的路径
    interceptor.excludePathPatterns("/login");


  }
}
