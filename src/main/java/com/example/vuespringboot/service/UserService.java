package com.example.vuespringboot.service;

import com.example.vuespringboot.mapper.UserMapper;
import com.example.vuespringboot.pojo.Page;
import com.example.vuespringboot.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

  @Autowired
  public UserMapper mapper;

  public List<String> list = new ArrayList<>();


  public List<User> pageUserList(Page page){
    List<User> users = mapper.selectList();
    int firstIndex = (page.getCurrPage() - 1) * page.getPageSize();
    int lastIndex = page.getCurrPage() * page.getPageSize();
    return users.subList(firstIndex,lastIndex);
  }


}
