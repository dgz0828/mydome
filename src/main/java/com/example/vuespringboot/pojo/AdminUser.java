package com.example.vuespringboot.pojo;

import lombok.Data;

@Data
public class AdminUser {
    private String username;

    private String password;

}
