package com.example.vuespringboot.pojo;

import lombok.Data;

@Data
public class Page {
  private int currPage;

  private int pageSize;
}
