package com.example.vuespringboot.controller;


import com.example.vuespringboot.mapper.UserMapper;
import com.example.vuespringboot.pojo.AdminUser;
import com.example.vuespringboot.pojo.Page;
import com.example.vuespringboot.pojo.User;
import com.example.vuespringboot.service.UserService;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

@RestController("/user")
@CrossOrigin
public class UserController {

  @Autowired
  public UserMapper userMapper;

  @Autowired
  public UserService service;


 /* @Autowired
  public RedisTemplate redisTemplate;*/

/*  @Autowired
  private RedissonClient redissonClient;*/


  @PostMapping("/update")
  public String update(MultipartFile file){
    System.out.println("进入文件上传方法");
    if (file.isEmpty()){
      return "上传失败";
    }
    String originalFilename = file.getOriginalFilename();
    System.out.println("文件名："+originalFilename);

    String suffixName = originalFilename.substring(originalFilename.lastIndexOf("."));
    System.out.println("后缀为："+suffixName);

    String filePath = "D:\\上传test";


    File folder = new File(filePath);

    try {
      file.transferTo(new File(folder,originalFilename));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }


    return "上传成功";



  }


/*  @PostMapping("/login")
  public boolean login(@RequestBody AdminUser user) {
    if (user.getUsername().equals("dgz") && user.getPassword().equals("123")) {
      //service.list.add("1");
      redisTemplate.opsForValue().set(user.getUsername(), user.getPassword());
      return true;
    } else {
      return false;
    }
  }*/


/*  @PostMapping("/tLogin")
  public boolean tLogin() {
    return redisTemplate.delete("dgz");
  }*/

  @GetMapping("/getUser/{id}")
  public User getUserById(@PathVariable("id") int id) {
    return userMapper.getUserById(id);
  }


  @PostMapping("/inset")
  @Transactional
  public int insetUser(@RequestBody User user) {
    System.out.println(user);
    return userMapper.insetUser(user);
  }

  @PostMapping("/list")
  @Transactional
  public List<User> selectList() {
    return userMapper.selectList();
  }


  @PostMapping("/delete")
  public Boolean deleteById(@RequestBody User user) {
    int delete = userMapper.delete(user.getId());
    return delete == 1;
  }

  public String sd(String s) {

    String[] split = s.split("");
    File file = new File("\\e");

    try {
      FileInputStream fileInputStream = new FileInputStream(file);
      int read = fileInputStream.read();


    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return "";
  }


  public List<User> pageList(Page page) {
    return service.pageUserList(page);
  }

}
