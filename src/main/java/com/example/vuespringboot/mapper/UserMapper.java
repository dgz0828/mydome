package com.example.vuespringboot.mapper;


import com.example.vuespringboot.pojo.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper {

    User getUserById(Integer id);

    int insetUser(User user);

    @Select("select * from user")
    List<User> selectList();

    int delete(Integer id);






}
