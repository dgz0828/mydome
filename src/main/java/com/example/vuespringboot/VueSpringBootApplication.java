package com.example.vuespringboot;

import com.example.vuespringboot.Interceptor.MyInterceptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.HandlerInterceptor;

@SpringBootApplication
/*@ComponentScan(basePackages = {"com.example.vuespringboot.mapper"})*/
public class VueSpringBootApplication {

  public static void main(String[] args) {
    SpringApplication.run(VueSpringBootApplication.class, args);
  }


}
